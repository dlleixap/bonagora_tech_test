package com.dlleixap.bonagora_davidlleixa.app.fragments;



import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import android.animation.Animator;
import android.animation.LayoutTransition;
import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dlleixap.bonagora_davidlleixa.app.R;
import com.dlleixap.bonagora_davidlleixa.app.data.CategoriesJson;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static android.widget.LinearLayout.LayoutParams;

/**
 * A simple {@link Fragment} subclass.
 *
 */
public class CategoriesFragment extends Fragment {

    private static final String TAG = "CategoriesFragment";
    private static final String NAME_JSON_FILE = "categories.json";

    private LinearLayout mLinearLayout;
    private LayoutParams mCategoryLayoutParams;
    private int mPadding;
    private List<CategoriesJson> mCategoriesJson;

    // Used to store the categories created while clicking
    // through the categories
    private List<CategoriesJson> mBackUpCategories;

    // Needed to load json file
    private Type mTypeToken;
    private Gson mGson;
    private String mCategories;

    // Tells size of the elements shown on the last
    // click for removing purposes on our LinearLayout
    private int mLastCategoriesSize;

    // Tells level of categories we are in
    private int mLevelCategories;

    public CategoriesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(final LayoutInflater inflater,
            final ViewGroup container, final Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater
                .inflate(R.layout.fragment_categories, container, false);

        // CHECK WHETHER SAVEDINSTANCE IS NULL OR NOT TO GET SAVED
        // VALUES
        if (savedInstanceState != null) {
            // GET NECESSARY VALUES
        } else {
            // GET INITIAL VALUES
        }

        // Set level categories to 0 Should check if savedInstanceState
        // is null or not and get the value
        mLevelCategories = 0;

        // Create TypeToken so that gson library can understand what
        // the json structure is going to be
        mTypeToken = new TypeToken<List<CategoriesJson>>() {}.getType();
        mGson = new Gson();

        // Get json elements using GSON library
        mCategories = loadJSONFromAsset(NAME_JSON_FILE);
        mCategoriesJson = mGson.fromJson(mCategories,
                mTypeToken);

        mLinearLayout = (LinearLayout) view.findViewById(R.id.categories_layout);

        // Create new instance of LayoutTransition and set it up
        LayoutTransition layoutTransition = new LayoutTransition();
        setUpLayoutTransition(layoutTransition);

        // Set layoutTransition to the layout we want to apply the transitions
        mLinearLayout.setLayoutTransition(layoutTransition);

        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();

        mCategoryLayoutParams = new LayoutParams(
                LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);

        mPadding = 16 * (int)displayMetrics.density;

        // Add first categories
        for (int i = 0; i < mCategoriesJson.size(); i++) {
            addCategoryToLayout(i, i, i, mCategoriesJson);
        }

        return view;
    }


    private void setUpLayoutTransition(LayoutTransition layoutTransition) {
        Animator animatorFadeIn = ObjectAnimator.ofFloat(null, "alpha", 0, 1);
        Animator animatorFadeOut = ObjectAnimator.ofFloat(null, "alpha", 1, 0);
        layoutTransition.setAnimator(LayoutTransition.DISAPPEARING,
                animatorFadeOut);
        layoutTransition.setAnimator(LayoutTransition.APPEARING, animatorFadeIn);
    }


    private void addCategoryToLayout(int id, int positionInJson, int positionInLayout,
            List<CategoriesJson> categoriesJson) {

        TextView textView = new TextView(getActivity());
        textView.setId(id);
        textView.setTextColor(
                getResources().getColor(android.R.color.white));
        textView.setLayoutParams(mCategoryLayoutParams);
        textView.setText(categoriesJson.get(positionInJson).getTitle());
        textView.setTextSize(18);
        textView.setClickable(true);
        textView.setPadding(mPadding, mPadding, mPadding, mPadding);
        mLinearLayout.addView(textView, positionInLayout);
        textView.setOnClickListener(new MyOnclickListener(id));
        Log.d(TAG, "added: " + textView.getText());
        Log.d(TAG, "with id: " + id);
    }


    private void reorderCategories(int id) {
        TextView textView = new TextView(getActivity());

        if (mLevelCategories == 0) {
            Log.d(TAG, "first");
            mBackUpCategories = new ArrayList<CategoriesJson>();
            mLastCategoriesSize = mCategoriesJson.size();
            textView.setId(-1);
            textView.setTextColor(
                    getResources().getColor(android.R.color.white));
            textView.setClickable(true);
            textView.setLayoutParams(mCategoryLayoutParams);
            textView.setText(getActivity().getString(R.string.all_products));
            textView.setTextSize(18);
            textView.setPadding(mPadding, mPadding, mPadding, mPadding);
            mLinearLayout.addView(textView, 0);
            textView.setOnClickListener(new MyOnclickListener(-1));
        }

        for (int i = mLastCategoriesSize+mLevelCategories; i > mLevelCategories ; i--) {

            if (id != mLinearLayout.getChildAt(i).getId()) {
                mLinearLayout.removeView(mLinearLayout.getChildAt(i));
            } else {
                ((TextView) mLinearLayout.getChildAt(i)).setId(
                        mLevelCategories);
                ((TextView) mLinearLayout.getChildAt(i)).setOnClickListener(
                        new MyOnclickListener(mLevelCategories));
            }
        }

        if (mLevelCategories == 0) {
            mBackUpCategories.add(mCategoriesJson.get(id));
            mCategoriesJson = mCategoriesJson.get(id).getSubCategories();

            for (int i = 0; i < mCategoriesJson.size(); i++) {
                addCategoryToLayout(mLevelCategories + i + 1, i, mLevelCategories + i + 2,
                        mCategoriesJson);
            }
        } else {
            mBackUpCategories.add(mCategoriesJson.get(id-mLevelCategories));
            mCategoriesJson = mCategoriesJson.get(id-mLevelCategories).getSubCategories();

            Log.d(TAG, "size :" + mCategoriesJson.size());

            for (int i = mLevelCategories; i < mCategoriesJson.size()+mLevelCategories; i++) {
                addCategoryToLayout(mLevelCategories + i, i-mLevelCategories, mLevelCategories + i + 1,
                        mCategoriesJson);
            }
        }

        // Increment level
        mLevelCategories++;

    }


    private String loadJSONFromAsset(String jsonFileName) {
        String json;
        try {
            InputStream is = getActivity().getResources().getAssets().open(jsonFileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // SAVE NECESSARY VALUES IN CASE THERE A CONFIGURATION CHANGE
    }

    /**
     * Inner Class that will handle clicks events on the Categories
     */
    private class MyOnclickListener implements View.OnClickListener {

        private int mId;

        private MyOnclickListener(int id) {
            mId = id;
        }

        @Override
        public void onClick(View view) {

            if (mId == -1) {
                // Clear all lists we have and set everything to the beginning
                mLevelCategories = 0;
                mBackUpCategories.clear();
                mCategoriesJson = mGson.fromJson(mCategories,
                        mTypeToken);
                mLinearLayout.removeAllViews();
                for (int i = 0; i < mCategoriesJson.size(); i++) {
                    addCategoryToLayout(i, i, i, mCategoriesJson);
                }

            } else {
                if (mId >= mLevelCategories) {
                    // We add the clicked category below the piled ones
                    // and show its subcategories
                    reorderCategories(mId);
                    if (mLevelCategories != 1) {
                        mLinearLayout.removeViewAt(
                                mLinearLayout.getChildCount() - 1);
                        mLastCategoriesSize = mCategoriesJson.size()-1;
                    } else {
                        mLastCategoriesSize = mCategoriesJson.size();
                }

                } else {
                    // Check if there's any backup categories in
                    // the position where id is telling

                    // HERE SHOULD GO IMPLEMENTATION TO TREAT CLICK
                    // WHEN ON ANY CATEGORY ON TOP OF THE ACTUAL ONE

                }
            }
        }
    }
}

package com.dlleixap.bonagora_davidlleixa.app.activities;

import com.dlleixap.bonagora_davidlleixa.app.R;
import com.dlleixap.bonagora_davidlleixa.app.fragments.CategoriesFragment;
import com.dlleixap.bonagora_davidlleixa.app.fragments.ProductsFragment;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;


public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Check whether the activity is using the layout version with
        // the fragment_container FrameLayout. If so, we must add the first fragment
        if (findViewById(R.id.categories_container) != null) {

            // However, if we're being restored from a previous state,
            // then we don't need to do anything and should return or else
            // we could end up with overlapping fragments.
            if (savedInstanceState != null) {
                return;
            }

            // Create an instance of CategoriesFragment
            CategoriesFragment categoriesFragment = new CategoriesFragment();

            // In case this activity was started with special instructions from an Intent,
            // pass the Intent's extras to the fragment as arguments
            //categoriesFragment.setArguments(getIntent().getExtras());

            // Add the fragment to the 'categories_container' FrameLayout
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.categories_container, categoriesFragment).commit();
        }

        // Do the same with the second fragment
        if (findViewById(R.id.products_container) != null) {

            if (savedInstanceState != null) {
                return;
            }

            // Create an instance of ProductsFragment
            ProductsFragment productsFragment = new ProductsFragment();

            //productsFragment.setArguments(getIntent().getExtras());

            // Add the fragment to the 'products_container' FrameLayout
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.products_container, productsFragment).commit();
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

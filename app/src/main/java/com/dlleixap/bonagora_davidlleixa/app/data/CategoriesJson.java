package com.dlleixap.bonagora_davidlleixa.app.data;

/**
 * Created by dlleixap on 15/05/14.
 */

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CategoriesJson {

    @SerializedName("title")
    private String mTitle;

    @SerializedName("children")
    private List<CategoriesJson> mSubCategories;

    public String getTitle() {
        return mTitle;
    }

    public List<CategoriesJson> getSubCategories() {
        return mSubCategories;
    }
}
